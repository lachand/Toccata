# ![Toccata logo](readme_files/toccata_logo.png) 

 Toccata is a system developed to support planning of pedagogical activities (scripting), seamless sharing of content and collaboration across people and devices, live
management of activities in the classroom, roaming for situations outside classrooms, resumption across sessions, and
resilience to unstable network conditions.

## Installation

### Requirements

You need to have at least node 6.9 and npm installed on your computer
```sh
# Debian based (no root needed)
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash
export NVM_DIR="$HOME/.nvm"
nvm install node
npm install -g @angular/cli
```

You also need to have a CouchDB installed on your computer http://docs.couchdb.org/en/2.1.1/install/unix.html

### CouchDB database

You need to have a working CouchDB http://docs.couchdb.org/en/2.1.1/install/index.html

## Development server

Run `npm install --dev` to install all dependencies

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Connection between CouchDB and Toccata

You need to create the following environment variables in your system according to your CouchDB configuration :
- URL_DB
- PORT_DB
- USERNAME_DB
- PASSWORD_DB

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.
