export const environment = {
  production: false,
  DB: "toccata-cscl",
  URL_DB: "http://10.42.0.1",
  USERBASE_DB: "",
  PASSWORD_DB: "",
  ROOM: "Demo",
  URL_PORT: "5984",
  envName: "ubicomp",
  message: "default username and password are teachercscl/teachercscl for teacher demonstration account"
};
