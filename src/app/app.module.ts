import 'zone.js';
import 'reflect-metadata';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { routes } from './app.routes';
import { AppComponent } from './app.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { OrderBy } from './external/orderBy';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { ServiceWorkerModule } from '@angular/service-worker';
import { DragulaModule } from 'ng2-dragula';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { MaterialDesignModule } from './materialDesign.module';
import { ServicesModule } from './services/services.module';
import { PagesModule } from './Pages/pages.module';
import { SharedModule } from './Shared/shared.module';
import { MenuComponent } from './menu/menu.component';
import { UserService } from './services/user.service';
import { ActivityService } from './services/activity.service';
import { ResourcesService } from './services/resources.service';
import { LoggedInGuard } from './verifications/logged-in.guards';
import { AppsService } from './services/apps.service';
import { DatabaseService } from './services/database.service';
import { LoggerService } from './services/logger.service';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    OrderBy,
  ],
  imports: [
    ServicesModule,
    CommonModule,
    MaterialDesignModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpModule,
    BrowserAnimationsModule,
    CKEditorModule,
    FlexLayoutModule,
    ServiceWorkerModule.register('/ngsw-worker.js'),
    DragulaModule.forRoot(),
    ScrollToModule.forRoot(),
    RouterModule.forRoot(routes, { useHash: true }),
    PagesModule,
    SharedModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy,
    },
    UserService,
    ActivityService,
    ResourcesService,
    LoggedInGuard,
    AppsService,
    DatabaseService,
    LoggerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
