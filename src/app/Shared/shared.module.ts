import { NgModule } from '@angular/core';
import { MaterialDesignModule } from '../materialDesign.module';
import { DialogConfirmationComponent } from './dialogConfirmation/dialogConfirmation.component';
import { DialogInformationComponent } from './dialogInformation/dialogInformation.component';

@NgModule({
  imports: [
    MaterialDesignModule
  ],
  providers: [
    DialogConfirmationComponent,
    DialogInformationComponent
  ],
  declarations: [
    DialogInformationComponent,
    DialogConfirmationComponent
  ],
  exports: [
    DialogConfirmationComponent,
    DialogInformationComponent
  ]
})
export class SharedModule {}
