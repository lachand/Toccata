import { Component, Inject, LOCALE_ID } from '@angular/core';
import { UserService } from './services/user.service';
import { environment } from '../environments/environment';


@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  languageList = [
    { code: 'en', label: 'English' },
    { code: 'fr', label: 'Français' }
  ];
  /**
   * Create the main app
   * @param userService
   */
  constructor(public userService: UserService, @Inject(LOCALE_ID) protected localeId: string) {
    console.info('%c ========================= ', 'color: #707000');
    console.info('%c Initialization of Toccata ', 'color: #707000');
    console.info('%c ========================= ', 'color: #707000');
    const lang = window.navigator.language;
    console.info(`%c Detected language:  ${lang}`, 'color: #707000');
    if ((lang !== 'fr-FR' && environment.envName === 'demo' && lang !== 'fr' && environment.envName === 'demo') && !window.location.href.includes('en.demo.toccata.education') && !window.location.href.includes('fr.demo.toccata.education')) {
      window.location.href = "https://en.demo.toccata.education";
      console.info(`%c Switching to english version of Toccata`, 'color: #707000');
    }
  }
}
