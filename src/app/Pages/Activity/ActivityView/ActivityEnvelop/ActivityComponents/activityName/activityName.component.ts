import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivityService } from '../../../../../../services/activity.service';
import { Subscription } from 'rxjs';

@Component({
  selector: "activity-name",
  templateUrl: "./activityName.component.html"
})
export class ActivityNameComponent implements OnInit, OnDestroy {
  activityName: any;
  subscription: Subscription;

  @Input() activityId;

  constructor(public activityService: ActivityService) {}

  ngOnInit(): void {
    this.subscription = this.activityService.changes.subscribe(change => {
      if (change['doc']._id === this.activityId) {
        this.activityName = change['doc'].name;
      }
    });
    this.activityService.getActivityInfos(this.activityId).then(res => {
      this.activityName = res["name"];
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
