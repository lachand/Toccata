import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { DialogConfirmationComponent } from '../../../../../../Shared/dialogConfirmation/dialogConfirmation.component';

@Component({
  selector: "create-edit-postit",
  templateUrl: "./createColumn.component.html"
})
export class CreateColumnComponent {
  dialogRef: MatDialogRef<any>;
  formTask: FormGroup;

  constructor(
    public formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog
  ) {
    this.formTask = this.formBuilder.group({
      taskName: ['', Validators.required],
    });
  }

  update() {
    console.log(this.formTask);
    this.dialogRef.close({ type: "column", value: this.formTask.value.taskName });
  }

  close() {
    this.dialogRef.close({ type: "close" });
  }
}
