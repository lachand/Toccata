import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivityService } from '../../../../../../services/activity.service';
import { LoggerService } from '../../../../../../services/logger.service';
import { Subscription } from 'rxjs';

@Component({
  selector: "activity-hide",
  templateUrl: "./activityHide.component.html"
})
export class ActivityHideComponent implements OnInit, OnDestroy {
  activityName: any;
  activityVisible: any;
  subscription: Subscription;

  @Input() activityId;

  constructor(
    public activityService: ActivityService,
    private logger: LoggerService
  ) {}

  ngOnInit(): void {
    this.subscription = this.activityService.changes.subscribe(change => {
      if (change['doc']._id === this.activityId) {
        this.activityName = change['doc'].name;
        this.activityVisible = change['doc'].visible;
      }
    });
    this.activityService.getActivityInfos(this.activityId).then(res => {
      this.activityName = res["name"];
      this.activityVisible = res["visible"];
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
