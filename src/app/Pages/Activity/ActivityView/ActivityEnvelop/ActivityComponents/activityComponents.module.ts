import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ApplicationInfosComponent } from './applicationInfos/applicationInfos.component';
import { ActivityAppsComponent } from './activityApps/activityApps.component';
import { ActivityAppsEditComponent } from './activityAppsEdit/activityAppsEdit.component';
import { ActivityChangeUsersComponent } from './activityChangeUsers/activityChangeUsers.component';
import { ActivityDescriptionEditComponent } from './activityDescriptionEdit/activityDescriptionEdit.component';
import { ActivityEditComponent } from './activityEdit/activityEdit.component';
import { ActivityHideComponent } from './activityHide/activityHide.component';
import { ActivityInfosComponent } from './activityInfos/activityInfos.component';
import { ActivityNameComponent } from './activityName/activityName.component';
import { ActivityNameEditComponent } from './activityNameEdit/activityNameEdit.component';
import { ActivityNewAppComponent } from './activityNewApp/activityNewApp.component';
import { ActivityNewRessourceComponent } from './activityNewRessource/activityNewRessource.component';
import { ActivityParticipantsEditComponent } from './activityParticipantsEdit/activityParticipantsEdit.component';
import { ActivityResourcesComponent } from './activityResources/activityResources.component';
import { ActivityResourcesApplicationsComponent } from './activityResourcesApplications/activityResourcesApplications.component';
import { ActivityResourceViewComponent } from './activityResourceView/activityResourceView.component';
import { ActivitySequenceEditComponent } from './activitySequenceEdit/activitySequenceEdit.component';
import { ActivitySequenceInfosComponent } from './activitySequenceInfos/activitySequenceInfos.component';
import { ActivityStepperComponent } from './activityStepper/activityStepper.component';
import { AppLoadingComponent } from './appLoading/appLoading.component';
import { AppNotesComponent } from './appNotes/appNotes.component';
import { ChronometreInfosComponent } from './chronometreInfos/chronometreInfos.component';
import { CreateEditPostitComponent } from './createEditPostit/createEditPostit.component';
import { ParticipantInfosComponent } from './participantsInfos/participantInfos.component';
import { PostitInfosComponent } from './postitInfos/postitInfos.component';
import { ResourceInfosComponent } from './resourceInfos/resourceInfos.component';
import { MaterialDesignModule } from '../../../../../materialDesign.module';
import { RouterModule } from '@angular/router';
import { ApplicationsModule } from './applications/applications.module';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { DragulaModule} from 'ng2-dragula';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { DialogResourceEditionComponent } from './resourceInfos/dialogResourceEdition/dialogResourceEdition.component';
import { CreateColumnComponent } from './createColumn/createColumn.component';
import { AngularEditorModule } from '@kolkov/angular-editor';

@NgModule({
  providers: [
    ActivityAppsComponent,
    ActivityAppsEditComponent,
    ActivityChangeUsersComponent,
    ActivityDescriptionEditComponent,
    ActivityEditComponent,
    ActivityHideComponent,
    ActivityInfosComponent,
    ActivityNameComponent,
    ActivityNameEditComponent,
    ActivityNewAppComponent,
    ActivityNewRessourceComponent,
    ActivityParticipantsEditComponent,
    ActivityResourcesComponent,
    ActivityResourcesApplicationsComponent,
    ActivityResourceViewComponent,
    ActivitySequenceEditComponent,
    ActivitySequenceInfosComponent,
    ActivityStepperComponent,
    ApplicationsModule,
    AppLoadingComponent,
    AppNotesComponent,
    DialogResourceEditionComponent,
    ApplicationInfosComponent,
    ChronometreInfosComponent,
    CreateEditPostitComponent,
    CreateColumnComponent,
    ParticipantInfosComponent,
    PostitInfosComponent,
    ResourceInfosComponent,
    ReactiveFormsModule,
    CommonModule,
    DragulaModule,
    CKEditorModule
  ],
  declarations: [
    ActivityAppsComponent,
    ActivityAppsEditComponent,
    ActivityChangeUsersComponent,
    ActivityDescriptionEditComponent,
    ActivityEditComponent,
    ActivityHideComponent,
    ActivityInfosComponent,
    ActivityNameComponent,
    ActivityNameEditComponent,
    ActivityNewAppComponent,
    ActivityNewRessourceComponent,
    ActivityParticipantsEditComponent,
    ActivityResourcesComponent,
    ActivityResourcesApplicationsComponent,
    ActivityResourceViewComponent,
    ActivitySequenceEditComponent,
    ActivitySequenceInfosComponent,
    ActivityStepperComponent,
    AppLoadingComponent,
    AppNotesComponent,
    ApplicationInfosComponent,
    ChronometreInfosComponent,
    CreateEditPostitComponent,
    CreateColumnComponent,
    ParticipantInfosComponent,
    PostitInfosComponent,
    ResourceInfosComponent,
    DialogResourceEditionComponent
  ],
  imports: [
    CommonModule,
    MaterialDesignModule,
    RouterModule,
    CKEditorModule,
    FormsModule,
    ReactiveFormsModule,
    DragulaModule,
    ScrollToModule,
    AngularEditorModule
  ],
  exports: [
    ActivityAppsComponent,
    ActivityAppsEditComponent,
    ActivityChangeUsersComponent,
    ActivityDescriptionEditComponent,
    ActivityEditComponent,
    ActivityHideComponent,
    ActivityInfosComponent,
    ActivityNameComponent,
    ActivityNameEditComponent,
    ActivityNewAppComponent,
    ActivityNewRessourceComponent,
    ActivityParticipantsEditComponent,
    ActivityResourcesComponent,
    ActivityResourcesApplicationsComponent,
    ActivityResourceViewComponent,
    ActivitySequenceEditComponent,
    ActivitySequenceInfosComponent,
    ActivityStepperComponent,
    ApplicationsModule,
    ApplicationInfosComponent,
    AppLoadingComponent,
    AppNotesComponent,
    ChronometreInfosComponent,
    CreateEditPostitComponent,
    CreateColumnComponent,
    ParticipantInfosComponent,
    PostitInfosComponent,
    ResourceInfosComponent,
    CKEditorModule,
    DragulaModule
  ],
  entryComponents: [
    ActivityChangeUsersComponent,
    CreateColumnComponent
  ]
})
export class ActivityComponentsModule {}
