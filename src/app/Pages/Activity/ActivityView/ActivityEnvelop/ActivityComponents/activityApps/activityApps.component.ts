import { ChangeDetectorRef, Component, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { AppLoadingComponent } from '../appLoading/appLoading.component';
import { UserService } from '../../../../../../services/user.service';
import { ActivityService } from '../../../../../../services/activity.service';
import { Subscription } from 'rxjs';

@Component({
  selector: "app-activity-apps",
  templateUrl: "./activityApps.component.html",
  styleUrls: ["./activityApps.component.scss"]
})
export class ActivityAppsComponent implements OnDestroy{
  user: UserService;
  dialog: any;
  subscription: Subscription;

  constructor(
    public activityService: ActivityService,
    public router: Router,
    dialog: MatDialog,
    private ref: ChangeDetectorRef
  ) {
    this.dialog = dialog;
    this.subscription = this.activityService.changes.subscribe(change => {
      if (!this.ref["destroyed"]) {
        this.ref.detectChanges();
      }
    });
  }

  loadApp() {
    const dialogRef = this.dialog.open(AppLoadingComponent);
    dialogRef.componentInstance.dialogRef = dialogRef;
  }

  unloadApp(appId) {
    //return this.activityService.apps.unloadApp(appId);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
