import { ChangeDetectorRef, Component, Input, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { AppsService } from '../../../../../../../services/apps.service';
import { ActivityService } from '../../../../../../../services/activity.service';
import { DatabaseService } from '../../../../../../../services/database.service';
import { isNullOrUndefined } from 'util';
import { ViewRef_ } from '@angular/core/src/view';
import { CreateEditPostitComponent } from '../../createEditPostit/createEditPostit.component';
import { MatDialog } from '@angular/material';
import { LoggerService } from '../../../../../../../services/logger.service';
import { Subscription } from 'rxjs';
import { CreateColumnComponent } from '../../createColumn/createColumn.component';
import { DragulaService } from 'ng2-dragula';

@Component({
  selector: "app-postit",
  templateUrl: "./postit.component.html",
  styleUrls: ["./postit.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class PostitComponent implements OnInit, OnDestroy {
  @Input() appId;

  theme: any;
  fields: any[];
  source: any;
  dataAdapter: any;
  columns: any;
  postits: any[];
  template: string;
  title: any;
  subscription: Subscription;
  subs = new Subscription();

  constructor(
    public appsService: AppsService,
    public activityService: ActivityService,
    public databaseService: DatabaseService,
    private ref: ChangeDetectorRef,
    public dialog: MatDialog,
    private logger: LoggerService,
    private dragulaService: DragulaService
  ) {

    dragulaService.destroy('POSTITS');
    dragulaService.createGroup('POSTITS', {
      moves: function (el: any, container: any, handle: any): any {
        if (el.classList.contains('undraggable')) {
          return false;
        }
        return true;
      }
    });

    this.subs.add(this.dragulaService.drop('POSTITS')
      .subscribe(({ name, el, target, source, sibling }) => {
        console.log(source.id, target.id, el);
        console.log(el.innerHTML);
        const nameP = el.innerHTML.split('<h4>')[1].split('</h4>')[0].replace(/\s/g, '');
        const valueP = el.innerHTML.split('<h4>')[1].split('</h4>')[1].split('</mat-card>')[0].replace(/\s/g, '');
        this.movePostit(source.id, target.id, nameP, valueP);
      }));


    this.subscription = this.databaseService.changes.subscribe(change => {
        if (
          change['type'] === "Application" &&
          change['doc']._id === this.appId
        ) {
          this.initialize();
        }
      if (
        this.ref !== null &&
        this.ref !== undefined &&
        !(this.ref as ViewRef_).destroyed
      ) {
        this.ref.detectChanges();
      }
    });
  }

  initialize() {
    this.appsService.getApplication(this.appId).then((res: string) => {
      const appRessources = res;
      console.log(appRessources);
      if (!isNullOrUndefined(appRessources['columns'])) {
        this.columns = appRessources['columns'];
      }
      if (!isNullOrUndefined(appRessources['postits'])) {
        this.postits = appRessources['postits'];
      }
      console.log(this.columns);
    });
  }

  ngOnInit(): void {
    this.initialize();
  }

  checkNull(elmt) {
    return isNullOrUndefined(elmt);
  }

  updatePostit(postit) {
    this.databaseService
      .getDocument(postit._id)
      .then(result => {
        result["estimation"] = postit["estimation"];
        result["label"] = postit["label"];
        this.logger.log(
          "UPDATE",
          this.activityService.activityLoaded._id,
          postit._id,
          "postit updated"
        );
        this.databaseService.updateDocument(result);
      })
      .catch(err => {
        this.logger.log(
          "CREATE",
          this.activityService.activityLoaded._id,
          postit._id,
          "postit created"
        );
        this.databaseService.addDocument(postit);
      });
  }

  deletePostit(id) {
    this.databaseService.getDocument(id).then(doc => {
      doc["_deleted"] = true;
      this.databaseService.updateDocument(doc);
    });
  }

  createOrEdit(postit) {
    const dialogRef = this.dialog.open(CreateEditPostitComponent, {
      data: { postit: postit }
    });
    dialogRef.componentInstance.dialogRef = dialogRef;
    dialogRef.afterClosed().subscribe(result => {
      if (result.type === "postit") {
        this.updatePostit(result.value);
      } else if (result.type === "delete") {
        this.logger.log(
          "DELETE",
          this.activityService.activityLoaded._id,
          postit.id,
          "postit deleted"
        );
        this.deletePostit(result.value);
      }
    });
  }

  addColumn() {
    const dialogRef = this.dialog.open(CreateColumnComponent);
    dialogRef.componentInstance.dialogRef = dialogRef;
    dialogRef.afterClosed().subscribe(result => {
      if (result.type === "column") {
        if (isNullOrUndefined(this.columns)) {
          this.columns = [];
        }
        this.columns.push({name: result.value, postits: []});
        console.log(this.columns);
        this.appsService.getApplication(this.appId).then(app => {
          app['columns'] = this.columns;
          this.appsService.updateApplication(app);
        });
      }
    });
  }

  isNullOrUndefined(columns: any[]) {
    return isNullOrUndefined(columns);
  }

  createPostIt(columnName) {
    const dialogRef = this.dialog.open(CreateEditPostitComponent);
    dialogRef.componentInstance.dialogRef = dialogRef;
    dialogRef.afterClosed().subscribe(result => {
      if (result.type === "postit") {
        for (const column of this.columns) {
          console.log(column['name'], columnName);
          if (column['name'] === columnName) {
            column['postits'].push({name: result.value.label, estimation: result.value.estimation});
          }
        }
        console.log(this.columns);
        this.appsService.getApplication(this.appId).then(app => {
          app['columns'] = this.columns;
          this.appsService.updateApplication(app);
        });
      }
    });
  }

  private movePostit(id: string, id2: string, name: string, value: string) {
    for (const column of this.columns) {
      if (column.name === id) {
        const index = column['postits'].indexOf({name: name, estimation: value});
        column['postits'].splice(index, 1);
      }
      if (column.name === id2) {
        column['postits'].push({name: name, estimation: value});
      }
    }
    console.log(this.columns);
    this.appsService.getApplication(this.appId).then(app => {
      app['columns'] = this.columns;
      this.appsService.updateApplication(app);
    });
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
    this.subscription.unsubscribe();
  }
}
