import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { AppsService } from '../../../../../../../services/apps.service';
import { LoggerService } from '../../../../../../../services/logger.service';
import { DatabaseService } from '../../../../../../../services/database.service';
import { ActivityService } from '../../../../../../../services/activity.service';
import { transition, trigger, useAnimation } from '@angular/animations';
import { pulse } from 'ng-animate';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { isNullOrUndefined } from 'util';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular';

@Component({
  selector: "app-text-editor",
  templateUrl: "./textEditor.component.html",
  styleUrls: ["./textEditor.component.scss"],
  animations: [
    trigger("pulse", [
      transition(
        "* => *",
        useAnimation(pulse, {
          params: { timing: 1, delay: 0 }
        })
      )
    ])
  ]
})
export class TextEditorComponent implements OnInit {

  editorOptions: any;
  public resource: any;
  latestSaveInMinute: number;
  pulse: any;
  class: string;
  private _appId;
  existResource = false;

  @Input()
  set appId(appId) {
    this._appId = appId;
    this.init();
  }

  get appId() { return this._appId; }
  public Editor = ClassicEditor;

  constructor(
    public applicationService: AppsService,
    private logger: LoggerService,
    private databaseService: DatabaseService,
    private activityService: ActivityService
  ) {
    this.init();
    this.class = "margin-left";

    this.editorOptions = {
      toolbar: [
        "heading",
        "|",
        "bold",
        "italic",
        "link",
        "bulletedList",
        "numberedList",
        "blockQuote"
      ],
      heading: {
        options: [
          {
            model: "paragraph",
            title: "Paragraph",
            class: "ck-heading_paragraph"
          },
          {
            model: "heading1",
            view: "h1",
            title: "Heading 1",
            class: "ck-heading_heading1"
          },
          {
            model: "heading2",
            view: "h2",
            title: "Heading 2",
            class: "ck-heading_heading2"
          }
        ]
      }
    };
    /*const autosave = setInterval( () => {this.save();} , 30000);*/
  }

  ngOnInit(): void {
    this.applicationService.getRessources(this.appId).then(res => {
      console.log(res);
      console.log(res["rows"]);
      this.databaseService.getDocument(res["rows"][0]["id"]).then(doc => {
        console.log(doc);
        this.resource = doc;
        this.existResource = true;
      });
    });
   this.init();
  }

  init() {
    this.latestSaveInMinute = 0;

    setInterval(() => {
      this.latestSaveInMinute++;
      if (this.latestSaveInMinute >= 5) {
        this.class = "margin-left pulse-button";
      } else {
        this.class = "margin-left";
      }
    }, 60000);

    this.applicationService.getRessources(this.appId).then(res => {
      console.log(res);
      console.log(res["rows"]);
      this.databaseService.getDocument(res["rows"][0]["id"]).then(doc => {
        console.log(doc);
        this.resource = doc;
      });
    });
  }

  onChange({ editor }: ChangeEvent) {
    this.resource.text = editor.getData();
  }

  save() {
    console.log("manualsave");
    console.log(this.resource);
    this.latestSaveInMinute = 0;
    this.databaseService.getDocument(this.resource._id).then(res => {
      console.log('getting doc');
      console.log(res);
      if (res["text"] !== this.resource.text) {
        res["text"] = this.resource.text;
        this.databaseService.updateDocument(res).then(() => {
          this.logger.log(
            "Update",
            this.activityService.activityLoaded.id,
            this.appId,
            "Save text editor"
          );
        });
      }
    });
  }

  isNullOrUndefined(elmt) {
    return isNullOrUndefined(elmt);
  }
}
