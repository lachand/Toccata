import { Component, Input, OnInit } from '@angular/core';
import { isNullOrUndefined } from 'util';

@Component({
  selector: "field-builder",
  template: `
    <div class="form-group row" [formGroup]="form">
      <div>
        <strong>
        {{index}}. {{ field.name }}
        </strong>
        <strong *ngIf="field?.type === 'checkbox'" i18n="@@multipleAnswers"> (plusieurs réponses) </strong>
        <strong *ngIf="field?.type === 'radio'" i18n="@@uniqueAnswer"> (une seule réponse) </strong>
        <strong class="text-danger" *ngIf="field.required">*</strong>
      </div>
      <div class="col-md-9" [ngSwitch]="field.type">
        <div *ngSwitchCase="'text'">
          <strong *ngIf="!isNullOrUndefined(field.minWords) && field.minWords > 0"> {{field.minWords}} <ng-container i18n="@@minWordsText">mots minimum.</ng-container> </strong>
          <strong *ngIf="!isNullOrUndefined(field.maxWords) && field.maxWords > 0"> {{field.maxWords}} <ng-container i18n="@@maxWordsText">mots maximum.</ng-container> </strong>
          <textbox [field]="field" [form]="form"></textbox>
        </div>
        <dropdown
          *ngSwitchCase="'dropdown'"
          [field]="field"
          [form]="form"
        ></dropdown>
        <checkbox
          *ngSwitchCase="'checkbox'"
          [field]="field"
          [form]="form"
        ></checkbox>
        <radio *ngSwitchCase="'radio'" [field]="field" [form]="form"></radio>
        <file *ngSwitchCase="'file'" [field]="field" [form]="form"></file>
        <div
          class="alert alert-danger my-1 p-2 fadeInDown animated"
          *ngIf="!isValid && isDirty"
        >
          {{ field.label }} <ng-container i18n="@@isRequired">est requis</ng-container>
        </div>
      </div>
    </div>
  `,
  styleUrls: ["field-builder.component.scss"]
})
export class FieldBuilderComponent implements OnInit {
  @Input() field: any;
  @Input() form: any;
  @Input() index: any;

  get isValid() {
    return this.form.controls[this.field.name].valid;
  }

  get isDirty() {
    return this.form.controls[this.field.name].dirty;
  }

  constructor() {}

  ngOnInit() {}

  isNullOrUndefined(elmt) {
    return isNullOrUndefined(elmt);
  }
}
