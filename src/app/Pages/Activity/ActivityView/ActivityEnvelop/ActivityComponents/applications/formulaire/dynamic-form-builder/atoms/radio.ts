import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: "radio",
  template: `
    <div [formGroup]="form">
      <div class="form-check" *ngFor="let opt of field.options">
        <mat-radio-button [value]="opt.key" style="margin-bottom: .5em;">{{opt.key}}</mat-radio-button>
      </div>
    </div>
  `
})
export class RadioComponent {
  @Input() field: any = {};
  @Input() form: FormGroup;
}
