import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'checkbox',
  template: `
    <div [formGroup]="form">
      <div [formGroupName]="field.name">
        <div *ngFor="let opt of field.options" class="form-check form-check" style="padding-bottom: .5em;">
          <label class="form-check-label">
            <mat-checkbox [formControlName]="opt.key" class="form-check-input" type="checkbox" [id]="opt.key" [value]="opt.key">
              {{opt.key}}
            </mat-checkbox>
          </label>
        </div>
      </div>

    </div>
  `
})
export class CheckBoxComponent {
  @Input() field: any = {};
  @Input() form: FormGroup;

  get isValid() {
    return this.form.controls[this.field.name].valid;
  }

  get isDirty() {
    return this.form.controls[this.field.name].dirty;
  }
}
