import { NgModule } from '@angular/core';
import { ActivityComponentsModule } from './ActivityComponents/activityComponents.module';
import { ActivityDialogsModule } from './ActivityDialogs/activityDialogs.module';

@NgModule({
  providers: [
    ActivityComponentsModule,
    ActivityDialogsModule
  ],
  exports: [
    ActivityComponentsModule,
    ActivityDialogsModule
  ]
})
export class ActivityEnvelopModule {}
