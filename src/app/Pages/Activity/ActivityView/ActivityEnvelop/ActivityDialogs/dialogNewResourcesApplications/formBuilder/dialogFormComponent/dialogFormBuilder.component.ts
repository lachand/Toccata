import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { elementType, FormElement } from '../formBuilder.type';
import { DragulaService } from 'ng2-dragula';

@Component({
  selector: "dialog-form-builder",
  templateUrl: "./dialoFormBuilder.component.html",
  styleUrls: ["./dialogFormBuilder.component.scss"]})

export class DialogFormBuilderComponent implements OnInit {

  formGroup: FormGroup;
  elType = elementType;

  constructor(private dialogRef: MatDialogRef<DialogFormBuilderComponent>,
              private dragulaService: DragulaService,
              private _formBuilder: FormBuilder,
              @Inject(MAT_DIALOG_DATA) public element: FormElement) {
  }

  ngOnInit() {
    this.formGroup = this._formBuilder.group({
      elementType: ['', Validators.required]
    });
  }

  isAnswer(index) {
    this.element.options[index].isAnswer = ! this.element.options[index].isAnswer;
  }

  addChoice() {
    this.element.options.push({
      name: `Choix ${this.element.options.length + 1}`,
      isAnswer: false});
  }

  removeChoice(choice) {
    this.element.options.splice(this.element.options.indexOf(choice), 1);
  }

  inputChangeChoice($event, index) {
    console.log($event.target.value);
    this.element.options[index]['name'] = $event.target.value;
  }

  inputChange($event, elmt) {
    console.log($event.target.value);
    if (elmt === 'text') {
      this.element.text = $event.target.value;
    } else if (elmt === 'name') {
      this.element.name = $event.target.value;
    }
  }

  inputChangeOption($event, option) {
    console.log($event.target.value);
    this.element[option] = $event.target.value;
    console.log(this.element[option]);
  }

  handleChange($event) {
    console.log($event);
  }
}
