export enum elementType {
  Checkbox,
  Input,
  Textarea,
  Radio
}

export interface FormElement {
  type: elementType;
  name: string;
  text: string;
  options: Array<{name: string, isAnswer: boolean}>;
  minWords?: number;
  maxWords?: number;
}
