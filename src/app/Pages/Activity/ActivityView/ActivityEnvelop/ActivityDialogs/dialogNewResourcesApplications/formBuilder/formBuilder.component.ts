import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DragulaService } from 'ng2-dragula';
import { elementType, FormElement } from './formBuilder.type';
import { Subscription } from 'rxjs';
import { DialogFormBuilderComponent } from './dialogFormComponent/dialogFormBuilder.component';
import { MatDialog, MatDialogConfig } from '@angular/material';

@Component({
  selector: "form-builder",
  templateUrl: "./formBuilder.component.html",
  styleUrls: ["./formBuilder.component.scss"]
})
export class FormBuilderComponent implements OnInit {
  @Input() formElements: Array<FormElement>;
  @Output() formElementsChanges = new EventEmitter<Array<FormElement>>();
  subs: Subscription;
  enum = elementType;

  constructor(private dragulaService: DragulaService,
              private dialog: MatDialog) {
    this.subs = new Subscription();
    //this.formElements = [];
    dragulaService.destroy('COPYABLE');
    dragulaService.createGroup('COPYABLE', {
      copy: (el, source) => {
        return source.id === 'form-builder-component';
      },
      accepts: (el, target, source, sibling) => {
        return target.id !== 'form-builder-component';
      }
    });

    this.subs.add(this.dragulaService.drop('COPYABLE')
      .subscribe(({ name, el, target, source, sibling }) => {
        if (source.id === 'form-builder-component') {
          console.log(el.id);
          let formElmt: FormElement = {
            name: '',
            type: elementType.Radio,
            options: [],
            text: '',
            minWords: 0,
            maxWords: 0,
          };
          switch (el.id) {
            case 'textarea':
              formElmt.name = 'Réponse longue';
              formElmt.type = elementType.Textarea;
              formElmt.minWords = 0;
              formElmt.maxWords = 0;
              formElmt.text = 'Texte long';
              break;
            case 'checkbox':
              formElmt.name = 'Question à choix multiples';
              formElmt.type = elementType.Checkbox;
              formElmt.options = [{name: 'Choix 1', isAnswer: false}, {name: 'Choix 2', isAnswer: false}];
              formElmt.text = 'Question à choix multiples';
              break;
            default:
            // code block
          }
          this.formElements.push(formElmt);
          this.formElementsChanges.emit(this.formElements);
          el.parentElement.removeChild(el);
        }
      })
    );
  }

  /**
   * Edit an element of the form
   * @param elmt The element to edit
   */
  editElmt(elmt) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = elmt;

    const dialogRef = this.dialog.open(DialogFormBuilderComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      this.formElementsChanges.emit(this.formElements);
    });
    return false;
  }

  /**
   * Delete an element of the form
   * @param elmt The element to delete
   */
  deleteElmt(elmt) {
    this.formElements.splice(this.formElements.indexOf(elmt, 1));
    this.formElementsChanges.emit(this.formElements);
  }

  ngOnInit() {
  }

}
