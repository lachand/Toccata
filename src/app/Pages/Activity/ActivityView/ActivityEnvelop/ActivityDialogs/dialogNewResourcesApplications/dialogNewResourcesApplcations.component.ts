import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { elementType, FormElement } from './formBuilder/formBuilder.type';
import { AppsService } from '../../../../../../services/apps.service';
import { ResourcesService } from '../../../../../../services/resources.service';
import { ActivityService } from '../../../../../../services/activity.service';
import { LoggerService } from '../../../../../../services/logger.service';

@Component({
  selector: "dialog-new-resources-applications",
  templateUrl: "./dialogNewResourcesApplications.component.html",
  styleUrls: ["./dialogNewResourcesApplications.component.scss"]
})
export class DialogNewResourcesApplcationsComponent {

  firstFormGroup: FormGroup;
  applicationTypeFormGroup: FormGroup;
  applicationInfosFormGroup: FormGroup;
  applicationVisibilityFormGroup: FormGroup;
  resourceTypeFormGroup: FormGroup;
  fileFormGroup: FormGroup;
  resourceInfosFormGroup: FormGroup;
  resourceVisibilityFormGroup: FormGroup;
  form: Array<FormElement>;
  elementType: any;
  enum = elementType;
  fileUpload: any;
  resourceType: any;
  isOptional = false;

  constructor(private _formBuilder: FormBuilder,
              private activityService: ActivityService,
              private appsService: AppsService,
              private resourcesService: ResourcesService,
              private logger: LoggerService,
              private dialogRef: MatDialogRef<DialogNewResourcesApplcationsComponent>) {}

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      elementType: ['', Validators.required]
    });
    this.applicationTypeFormGroup = this._formBuilder.group({
      applicationType: ['', Validators.required]
    });
    this.applicationInfosFormGroup = this._formBuilder.group({
      chronometreValue: [''],
      appName: ['', Validators.required]
    });
    this.resourceTypeFormGroup = this._formBuilder.group({
      resourceType: ['', Validators.required]
    });
    this.resourceInfosFormGroup = this._formBuilder.group({
      resourceName: ['', Validators.required],
      url: ['']
    });
    this.resourceVisibilityFormGroup = this._formBuilder.group({
      stepOrActivity: ['', Validators.required]
    });
    this.applicationVisibilityFormGroup = this._formBuilder.group({
      stepOrActivity: ['', Validators.required]
    });
    this.fileFormGroup = this._formBuilder.group({
      file: ['', Validators.required]
    });
    this.form = [];
  }

  print() {
    console.log(this.firstFormGroup.get('elementType').value);
  }

  choose(value) {
    this.applicationTypeFormGroup.patchValue({
      applicationType: value
      }
    );
  }

  onFileChange(event) {
    let reader = new FileReader();

    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      console.log(file);
      reader.readAsDataURL(file);

      reader.onload = () => {
        this.fileFormGroup.patchValue({
          file: file
        });

        console.log(reader.result);
      };
    }
  }

  /**
   * Create a new application and add it to current activity
   */
  newApp() {
      const options = {};
      const json = [];
      let url = "";
      if (this.applicationTypeFormGroup.get('applicationType').value === 'Chronomètre') {
        options["time"] = this.applicationInfosFormGroup.get('chronometreValue').value;
      }

      if (this.applicationTypeFormGroup.get('applicationType').value === 'Formulaire') {
        console.log(this.form);
        for (const elmt of this.form) {
          console.log(elmt);
          console.log(elmt.type);
          console.log(this.enum.Textarea);
          if (elmt.type === this.enum.Textarea) {
            const elmt_text = {
              'type': 'text',
              'name': elmt.name,
              'label': elmt.text,
              'value': '',
              minWords: elmt.minWords,
              maxWords: elmt.maxWords,
              'required': true
            };
            json.push(elmt_text);
          }

          if (elmt.type === this.enum.Checkbox || elmt.type === this.enum.Radio) {
            const opt = [];
            let cpt = 0;
            for (const option of elmt.options) {
              opt.push({ key: option.name, label: option.name, isAnswer: option.isAnswer });
              if (option.isAnswer) {
                cpt++;
              }
            }
            if (cpt > 1) {
              const elmt_checkbox = {
                'type': 'checkbox',
                'name': elmt.name,
                'options': opt,
                'required': true
              };
              json.push(elmt_checkbox);
            } else if (cpt <= 1) {
              const elmt_radio = {
                'type': 'radio',
                'name': elmt.name,
                'options': opt,
                'required': true
              };
              json.push(elmt_radio);
            }
          }
        }
      }

      const appToAdd = {
        type: this.applicationTypeFormGroup.get('applicationType').value,
        provider: this.applicationTypeFormGroup.get('applicationType').value,
        name: this.applicationInfosFormGroup.get('appName').value,
        options: options,
        json: json,
        url: url
      };
      let id;
      if (this.applicationVisibilityFormGroup.get('stepOrActivity').value === "step") {
        id = this.activityService.activityLoaded._id;
      } else {
        id = this.activityService.activityLoaded.parent;
      }
      this.activityService.getActivityInfos(id).then(activity => {
        this.appsService
          .createApp(appToAdd, id, activity["dbName"])
          .then(app => {
            console.log(app);
            this.logger.log("CREATE", id, app["id"], "application created");
            if (this.applicationTypeFormGroup.get('applicationType').value === "Editeur de texte") {
              let resource = {
                _id: `ressource_${app["id"]}`,
                documentType: "Ressource application",
                application: app["id"],
                applicationType: "Editeur de texte",
                ressourceType: "Text",
                text: "",
                dbName: activity["dbName"],
                parent: activity["dbName"]
              };
              this.activityService.database.addDocument(resource).then(() => {
                this.dialogRef.close();
              });
            } else {
              this.dialogRef.close();
            }
          });
      });
    }

  newResource() {
      let id;
      if (this.resourceVisibilityFormGroup.get('stepOrActivity').value === "step") {
        id = this.activityService.activityLoaded._id;
      } else {
        id = this.activityService.activityLoaded.parent;
      }
      if (this.resourceTypeFormGroup.get('resourceType').value === "document") {
        this.resourcesService
          .createResource(this.fileFormGroup.get('file').value, id, this.resourceInfosFormGroup.get('resourceName').value)
          .then(res => {
            this.logger.log("CREATE", id, id, "resource created");
            this.dialogRef.close();
          });
      } else if (this.resourceTypeFormGroup.get('resourceType').value === "webPage") {
        let url: String = this.resourceInfosFormGroup.get('url').value;
        if (url.includes('youtube')) {
          url = url.replace('watch?v=', 'embed/');
        }
        const ressource = {
          name:  this.resourceInfosFormGroup.get('resourceName').value,
          value: url,
          type: "url"
        };
        this.resourcesService
          .createResource(ressource, id, this.resourceInfosFormGroup.get('resourceName').value)
          .then(res => {
            this.logger.log("CREATE", id, id, "resource created");
            this.dialogRef.close();
          });
      }
  }

}
