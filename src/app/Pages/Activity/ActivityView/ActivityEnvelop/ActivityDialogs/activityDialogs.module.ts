import { NgModule } from '@angular/core';
import { DialogNewResourcesApplcationsComponent } from './dialogNewResourcesApplications/dialogNewResourcesApplcations.component';
import { DialogNewRessourceComponent } from './dialogNewRessource/dialognewRessource.component';
import { DialogResourceOpenedComponent } from './dialogResourceOpened/dialogResourceOpened.component';
import { DialogTextEditionComponent } from './dialogTextEditor/dialogTextEdition.component';
import { ReactiveFormsModule, FormsModule} from '@angular/forms';
import { MaterialDesignModule } from '../../../../../materialDesign.module';
import { CommonModule } from '@angular/common';
import { FormBuilderComponent } from './dialogNewResourcesApplications/formBuilder/formBuilder.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { DialogFormBuilderComponent } from './dialogNewResourcesApplications/formBuilder/dialogFormComponent/dialogFormBuilder.component';
import { DragulaModule } from 'ng2-dragula';
import { FlexModule } from '@angular/flex-layout';
import { AngularEditorModule } from '@kolkov/angular-editor';

@NgModule({
  providers: [
    DialogNewResourcesApplcationsComponent,
    DialogNewRessourceComponent,
    DialogResourceOpenedComponent,
    DialogTextEditionComponent,
    DialogFormBuilderComponent
  ],
  declarations: [
    DialogNewResourcesApplcationsComponent,
    DialogNewRessourceComponent,
    DialogResourceOpenedComponent,
    DialogTextEditionComponent,
    DialogFormBuilderComponent,
    FormBuilderComponent
  ],
  entryComponents: [
    DialogNewResourcesApplcationsComponent,
    DialogNewRessourceComponent,
    DialogResourceOpenedComponent,
    DialogTextEditionComponent,
    DialogFormBuilderComponent,
    FormBuilderComponent
  ],
  imports: [
    ReactiveFormsModule,
    MaterialDesignModule,
    FormsModule,
    CommonModule,
    CKEditorModule,
    DragulaModule,
    FlexModule,
    AngularEditorModule
  ],
  exports: [
    DialogNewResourcesApplcationsComponent,
    DialogNewRessourceComponent,
    DialogResourceOpenedComponent,
    DialogTextEditionComponent,
    DialogFormBuilderComponent,
    CKEditorModule
  ]
})
export class ActivityDialogsModule {}
