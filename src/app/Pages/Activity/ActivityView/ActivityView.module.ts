import { NgModule } from '@angular/core';
import { ActivityViewComponent } from './activityView.component';
import { ActivityEnvelopModule } from './ActivityEnvelop/ActivityEnvelop.module';
import { ActivityResourcesApplicationsModule } from './ActivityResourcesApplications/activityResourcesApplications.module';
import { FlexModule } from '@angular/flex-layout';
import { MaterialDesignModule } from '../../../materialDesign.module';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    ActivityEnvelopModule,
    ActivityResourcesApplicationsModule,
    FlexModule,
    MaterialDesignModule,
    CommonModule
  ],
  providers: [
    ActivityViewComponent,
    ActivityEnvelopModule,
    ActivityResourcesApplicationsModule
  ],
  declarations: [
    ActivityViewComponent
  ],
  exports: [
    ActivityViewComponent,
    ActivityEnvelopModule,
    ActivityResourcesApplicationsModule
  ]
})
export class ActivityViewModule {}
