import { NgModule } from '@angular/core';
import { ApplicationLaunchedComponent } from './applicationLaunched/applicationLaunched.component';
import { DialogApplicationLaunchedComponent } from './dialogApplicationLaunched/dialogApplicationLaunched.component';
import { ResourceOpenedComponent } from './resourceOpened/resourceOpened.component';
import { MaterialDesignModule } from '../../../../materialDesign.module';
import { ApplicationsModule } from '../ActivityEnvelop/ActivityComponents/applications/applications.module';
import { CommonModule } from '@angular/common';

@NgModule({
  providers: [
    ApplicationLaunchedComponent,
    DialogApplicationLaunchedComponent,
    ResourceOpenedComponent
  ],
  declarations: [
    ApplicationLaunchedComponent,
    DialogApplicationLaunchedComponent,
    ResourceOpenedComponent
  ],
  imports: [
    MaterialDesignModule,
    ApplicationsModule,
    CommonModule
  ],
  exports: [
    ApplicationLaunchedComponent,
    DialogApplicationLaunchedComponent,
    ResourceOpenedComponent
  ]
})
export class ActivityResourcesApplicationsModule {}
