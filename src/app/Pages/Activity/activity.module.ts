import { NgModule } from '@angular/core';
import { ActivityGroupsModule } from './ActivityGroups/activityGroups.module';
import { ActivityViewModule } from './ActivityView/ActivityView.module';

@NgModule({
  providers: [
    ActivityGroupsModule,
    ActivityViewModule
  ],
  exports: [
    ActivityViewModule,
    ActivityGroupsModule
  ]
})
export class ActivityModule {}
