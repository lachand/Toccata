import { ChangeDetectorRef, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { AppsService } from '../../../../services/apps.service';
import { Subscription } from 'rxjs';

@Component({
  selector: "application-infos-teacher",
  templateUrl: "./applicationInfosTeacher.component.html"
})
export class ApplicationInfosTeacherComponent implements OnInit, OnDestroy {
  @Input() applicationId;
  application: any;
  currentLoaded: any;
  subscription: Subscription;

  constructor(
    public appsService: AppsService,
    private ref: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.subscription = this.appsService.changes.subscribe(change => {
      if (this.applicationId === change.doc._id) {
        this.application = change.doc;
      }
    });
    this.appsService
      .getApplicationInfos(this.applicationId)
      .then(applicationInfos => {
        this.application = applicationInfos;
      });
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
