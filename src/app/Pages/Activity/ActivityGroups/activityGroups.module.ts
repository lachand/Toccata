import { NgModule } from '@angular/core';
import { ActivityInfosTeacherComponent } from './activityInfosTeacher/activityInfosTeacher.component';
import { ApplicationInfosTeacherComponent } from './applicationInfosTeacher/applicationInfosTeacher.component';
import { ViewDuplicatesComponent } from './viewDuplicates/viewDuplicates.component';
import { DialogDuplicateNameComponent } from './viewDuplicates/dialogDuplicateName/dialogDuplicateName.component';
import { ActivityComponentsModule } from '../ActivityView/ActivityEnvelop/ActivityComponents/activityComponents.module';
import { CommonModule } from '@angular/common';
import { MaterialDesignModule } from '../../../materialDesign.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  providers: [
    ActivityInfosTeacherComponent,
    ApplicationInfosTeacherComponent,
    ViewDuplicatesComponent,
    DialogDuplicateNameComponent
  ],
  declarations: [
    ActivityInfosTeacherComponent,
    ApplicationInfosTeacherComponent,
    ViewDuplicatesComponent,
    DialogDuplicateNameComponent
  ],
  imports: [
    ActivityComponentsModule,
    CommonModule,
    MaterialDesignModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    ActivityInfosTeacherComponent,
    ApplicationInfosTeacherComponent,
    ViewDuplicatesComponent,
    DialogDuplicateNameComponent,
  ],
  entryComponents: [
    DialogDuplicateNameComponent
  ]
})
export class ActivityGroupsModule {}
