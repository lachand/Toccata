import { NgModule } from '@angular/core';
import { LoginSigninModule } from './LoginSignin/loginSignin.module';
import { MyActivitiesModule } from './MyActivities/myActivities.module';
import { ActivityModule } from './Activity/activity.module';
import { FlexLayoutModule, FlexModule } from '@angular/flex-layout';

@NgModule({
  providers: [
    LoginSigninModule,
    MyActivitiesModule,
    ActivityModule
  ],
  exports: [
    LoginSigninModule,
    MyActivitiesModule,
    ActivityModule
  ]
})
export class PagesModule {}
