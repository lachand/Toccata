import { NgModule } from '@angular/core';
import { ActivityModule } from '../Activity/activity.module';
import { MaterialDesignModule } from '../../materialDesign.module';
import { CommonModule } from '@angular/common';
import { MyActivitiesComponent } from './myActivities.component';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  imports: [
    ActivityModule,
    CommonModule,
    MaterialDesignModule,
    FlexLayoutModule
  ],
  providers: [
    MyActivitiesComponent
  ],
  declarations: [
    MyActivitiesComponent
  ],
  exports: [
    MyActivitiesComponent
  ]
})
export class MyActivitiesModule {}
