import { MaterialDesignModule } from '../../materialDesign.module';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { SigninComponent } from './signin.component';
import { DialogInformationComponent } from '../../Shared/dialogInformation/dialogInformation.component';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  providers: [
    LoginComponent,
    SigninComponent
  ],
  declarations: [
    LoginComponent,
    SigninComponent
  ],
  imports: [
    MaterialDesignModule,
    ReactiveFormsModule,
    CommonModule,
    FlexLayoutModule
  ],
  entryComponents: [
    DialogInformationComponent
  ],
  exports: [
    LoginComponent,
    SigninComponent
  ]
})
export class LoginSigninModule {}
