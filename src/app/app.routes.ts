import { LoggedInGuard } from './verifications/logged-in.guards';
import { MyActivitiesComponent } from './Pages/MyActivities/myActivities.component';
import { ActivityAppsComponent } from './Pages/Activity/ActivityView/ActivityEnvelop/ActivityComponents/activityApps/activityApps.component';
import { ExternalAppComponent } from './Pages/Activity/ActivityView/ActivityEnvelop/ActivityComponents/applications/external/externalApp.component';
import { ActivityEditComponent } from './Pages/Activity/ActivityView/ActivityEnvelop/ActivityComponents/activityEdit/activityEdit.component';
import { ActivityViewComponent } from './Pages/Activity/ActivityView/activityView.component';
import { ViewDuplicatesComponent } from './Pages/Activity/ActivityGroups/viewDuplicates/viewDuplicates.component';
import { LoginComponent } from './Pages/LoginSignin/login.component';
import { SigninComponent } from './Pages/LoginSignin/signin.component';

export const routes = [
  { path: "login", component: LoginComponent },
  { path: "activities", component: MyActivitiesComponent },
  { path: "", redirectTo: "/activities", pathMatch: "full" },
  {
    path: "duplicates/:id",
    component: ViewDuplicatesComponent,
    canActivate: [LoggedInGuard]
  },
  {
    path: "activity_edit/:id",
    component: ActivityEditComponent,
    canActivate: [LoggedInGuard]
  },
  {
    path: "activity_view/:id",
    component: ActivityViewComponent,
    canActivate: [LoggedInGuard]
  },
  {
    path: "activity_apps/:id",
    component: ActivityAppsComponent /**, canActivate: [LoggedInGuard]**/,
    children: [
      /**
      {
        path: "Externe/:id",
        component: ExternalAppComponent,
        outlet: "apps"
      },
      {
        path: "Editeur de texte/:id",
        component: ExternalAppComponent,
        outlet: "apps"
      },
      {
        path: "Feuille de calcul/:id",
        component: ExternalAppComponent,
        outlet: "apps"
      }
       **/
    ]
  },
  { path: "inscription", component: SigninComponent }
];
