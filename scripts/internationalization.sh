#!/bin/bash
for lang in en fr; do
ng build --output-path=dist/$lang --aot --configuration=$1 --i18n-file=src/locale/messages.$lang.xlf --i18n-format=xlf --i18n-locale=$lang;
done
